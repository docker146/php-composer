FROM php:7.4-alpine

RUN apk add --no-cache curl \
					git \
					freetype-dev \
					jpeg-dev \
					libpng-dev \
					libzip-dev \
					php \
					php-curl \
					php-dom \
					php-fileinfo \
					php-gd \
					php-iconv \
					php-json \
					php-mbstring \
					php-openssl \
					php-phar \
					php-simplexml \
					php-sockets \
					php-tokenizer \
					php-xml \
					php-xmlwriter
					
RUN docker-php-ext-configure gd --with-freetype --with-jpeg

RUN docker-php-ext-install pdo_mysql gd

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer
